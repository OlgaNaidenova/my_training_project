"""book.py
Задание:
Создайте класс, описывающий книгу.
Он должен содержать информацию об авторе, названии, годе издания и жанре.
Создайте несколько разных книг.
Определите для него операции проверки на равенство и неравенство,
методы __repr__ и __str__"""

class Book:
    """Create class book"""
    def __init__(self, author, name, year, genre):
        """Consrtuctor
        author attribute
        book name attribute
        year of publication attribute
        genre attribute
        """
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre

    def __repr__(self):
        """Representation of book list"""
        return f'Book name: {self.name}, author: {self.author}, year: {self.year}, genre: {self.genre}\n'

    def __str__(self):
        """Description of book for user"""
        return f'The {self.name} book by {self.author} author. Genre: {self.genre}. Date published of book: {self.year}'

    def __eq__(self, other):
        """Books comparison
        To compare two books with __ne__ function
        """
        print(f'Compare two books with __eq__ function: {self.name} and {other.name}:')
        if self.name == other.name and self.author == other.author:
            print('These books are the same')
            return True
        print('These books are not the same')
        return False

    def  __ne__(self, other):
        """Inequality of Books
        To compare two books with __ne__ function
        """
        print(f'Compare two books with __ne__ function: {self.name} and {other.name}:')
        if self.name != other.name and self.author != other.author:
            print('These books are different')
            return True
        print('These books are not different')
        return False

# Create instances of class Book (books):
master_and_margarita = Book("M.Bulgakov", "Master and Margarita", 2018, "Romance")
paint = Book("P.Gnedich", "World paintal art", 2011, "pirtorial art")
child = Book("G.Doring", "Child from 0 to 6", 1992, "child health")
knitting = Book("M.Maximova", "Knitting alphabet", 2014, "needlework")

#Create list of books
BOOKS = [master_and_margarita, paint, child, knitting]
print(BOOKS)
print()

#Print description of 'master and margarita' book
print(master_and_margarita)
print()

print(1)
#Compare two books with__eq__ function
paint == master_and_margarita
print(2)
paint == paint

print(3)
# Compare two books with __ne__ function
knitting != child
print(4)
knitting != knitting