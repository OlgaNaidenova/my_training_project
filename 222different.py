"""222different.py
Задание:
 Опишите классы графического объекта, прямоугольника и объекта,
 который может обрабатывать нажатия мыши. Опишите класс кнопки.
 Создайте объект кнопки и обычного прямоугольника.
 Вызовите метод нажатия на кнопку. """

class Graphic():
    """Create class Graphic obj"""
    color = 'white'

class Rectangle(Graphic):
    """Create subclass Rectangle"""
    def __init__(self, side_a, side_b):
        """Constructor of class Rectangle with side a and side b"""
        self.side_a = side_a
        self.side_b = side_b

    def __repr__(self):
        """Representation for rectangle"""
        return f'Rectangle {self.side_a}, {self.side_b}'

class Clickable():
    """Create class Clickable"""
    def click(self):
        """click method"""
        print('Clicked')

class Button(Rectangle, Clickable):
    """Create subclass Button
    __repr__ and __init__ methods are inherited from Rectangle class
    click method is inherited from Clickable class"""

#Create instance of Button class with side_a=2 and side_b=4
button = Button(2, 4)
#Call click method for button instance
button.click()
#Call __repr__ method for button instance
print(button)
#Create instance of Rectangle class with side_a=3 and side_b=4
f = Rectangle(3, 4)
#Call __repr__ method for f instance
print(f)
