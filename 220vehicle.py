"""220vehicle.py
Задание:
Создайте иерархию классов транспортных средств. В общем классе опишите общие для всех
транспортных средств поля, в наследниках – специфичные для них. Создайте несколько экземпляров.
Выведите информацию о каждом транспортном средстве.
"""

class Vehicle:
    """Create base class: Vehicle"""
    wheels = 4    # base wheels attribute
    people = None   # base people attribute

    def move(self):
        """move function of base class"""
        print('Vehicle can move')

    def print(self):
        """print function of base class"""
        print(f'has {self.wheels} wheels')

class Pub(Vehicle):
    """Create subclass Pub"""
    people = "many"     # people attribute of subclass

    def carries_people(self):
        """carries_people function of subclass"""
        print(f'Public vehicle carries {self.people} people')

class Flying(Pub):
    """Create subclass Flying"""
    wheels = 3    # wheels attribute of subclass
    def fly(self):
        """fly function of subclass"""
        print(f'Flying public vehicle carries {self.people} people by air')

class Pri(Vehicle):
    """Create subclass Pri"""
    people = "less than 5"  # people attribute of subclass

    def carries_5people(self):
        """carries_5people function of subclass"""
        print(f'Private vehicle carries {self.people} people')

class TwoWheeled(Pri):
    """Create subclass Pri"""
    wheels = 2      # wheels attribute of subclass
    people = "less than 2"  # people attribute of subclass

    def carries_2people(self):
        """carries_2people function of subclass"""
        print(f'Two-wheeled private vehicle carries {self.people} people')

#Create instances of classes
bus = Pub()
plane = Flying()
car = Pri()
moto = TwoWheeled()

#print info about each instance
print('Bus')
bus.print()
bus.carries_people()
print()

print('Plane')
plane.print()
plane.fly()
print()

print('Car')
car.move()
car.print()
car.carries_5people()
print()

print('Moto')
moto.move()
moto.print()
moto.carries_2people()
