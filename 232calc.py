"""232calc.py
Задание:
 Напишите программу-калькулятор, которая поддерживает следующие операции:
 сложение, вычитание, умножение, деление и возведение в степень.
 Программа должна выдавать сообщения об ошибке и
 продолжать работу при вводе некорректных данных,
 делении на ноль и возведении нуля в отрицательную степень
 """

#class Calc():
#    """Create class calc"""
#    pass

#Create some numbers for arifmetic operations
B = 0
C = -2
D = 2

try:
    #Enter a from userinput
    a = int(input('Enter number a:'))
    print('a + b =', a+B)
    print('a - b =', a-B)
    print('a * b =', a*B)
    try:
        print('a / b =', a/B)
    except ZeroDivisionError as e:
        print('Error: ', e)
    print('a ^ d =', a**D)
    try:
        print('b ^ c', B**C)
    except ZeroDivisionError as e:
        print('Error: ', e)
except ValueError as v:
    print('Error: ', v)
print('Calculator program is over.')
