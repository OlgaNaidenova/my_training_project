"""Temperature.py
Создайте класс, описывающий температуру и позволяющий задавать
и получать температуру по шкале Цельсия и Фаренгейта,
причём данные могут быть заданы в одной шкале, а получены в другой.
Note: I used static method for this task.
"""

class Temperature:
    """Create class Temperature"""
    def __init__(self, temp, gr):
        """Constructor
        temp attribute
        gr attribute"""
        self.temp = temp
        self.gr = gr

    def __str__(self):
        """Description of temperature for user"""
        return f'The entered temperature is {self.temp} {self.gr}.'

    def print_c(self):
        """Print temperature in Сelsius"""
        print(self.temp, self.gr)

    def print_f(self):
        """Print temperature in Fahrenheit"""
        print(self.temp, self.gr)

    @staticmethod
    def from_t(temp, gr):
        """Conversion of temperature"""
        if gr == "F":
            return (temp-32)*5/9    #Fahrenheit to Сelsius
        elif gr == "C":
            return (temp*9/5)+32    #Сelsius to Fahrenheit
        return NotImplemented

#Create instances of class Temperature
c = Temperature(10, "C")
f = Temperature(86, "F")

#Print message about temperature for user
print(c)
print(f)
print()

#Print temperature without user message
c.print_c()
f.print_f()
print()

#Convert temperature and print message
print(f'The temperature 0 C = {Temperature.from_t(0,"C")} F')
print(f'The temperature 68 F = {Temperature.from_t(68,"F")} C')