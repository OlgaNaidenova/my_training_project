"""230function_except
 Задание:
  Опишите свой класс исключения.
  Напишите функцию, которая будет выбрасывать данное исключение,
  если пользователь введёт определённое значение,
  и перехватите это исключение при вызове функции.
 """

class MyException(Exception):
    """Create class MyException"""
    def email_nok(self):
        """email_nok method to print error for user 'Email is not ok!' """
        print('Email is not ok!')

#Ask user to enter email
email = input('Enter your email:')
try:
    #Find @ in entered email to verify that email is ok
    if email.find('@') != -1:
        print('Email accepted')
    else:
        #Generate Exception if @ is not found
        raise MyException("error")
except Exception as e:
    #Call email_nok method of MyException class
    e.email_nok()
    print('Please try again.')
