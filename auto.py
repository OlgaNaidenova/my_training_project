"""auto.py
Задание:
Создайте класс, описывающий автомобиль.
Создайте класс автосалона, содержащий в себе список автомобилей,
доступных для продажи, и функцию продажи заданного автомобиля.
"""

class Auto:
    """Create class auto"""
    def __init__(self, name, volume, price):
        """Consrtuctor
        car name attribute
        engine volume attribute
        price attribute
        """
        self.name = name
        self.volume = volume
        self.price = price


class Salon:
    """Create class autosalon"""
    def __init__(self):
        """"Consrtuctor
        create empty list
        """
        self.list_for_sale = []

    def for_sale(self, car):
        """add inctances of Auto class(cars) to list of Salon"""
        self.list_for_sale.append(car)    # add car to salon list 'list_for_sale'

    def price_list(self):
        """Print Price list of available cars in auto salon for sale"""
        for i,car in enumerate(self.list_for_sale, start=1):
            print(f'{i}.{car.name} - {car.price} $')

#        i = 1
#        for car in self.list_for_sale:
#            print(i, ". ", car.name, " - ", car.price, "$")
#            i += 1

    def sale(self, car_sale):
        """Methon sale car:
        Find car in salon list that number is selected by user in the price list.
        Print sale message for user.
        Delete selected car from salon list.
        """
        car = self.list_for_sale[int(car_sale)-1]
        print(f"Congratulations! You have buyed the {car.name} car for {car.price} $")
        self.list_for_sale.pop(int(car_sale)-1)

# Create instances of class Auto (cars):
SPORT = Auto("Sport", 2.8, 100)
UNIVERSAL = Auto("Universal", 1.8, 80)
MINI = Auto("Mini", 1.6, 50)
JEEP = Auto("Jeep", 3.0, 120)
SEDAN = Auto("Sedan", 1.7, 60)

# Create instance of class Salon:
SALON = Salon()

# Add created cars to salon list:
print("Cars for sale:")
SALON.for_sale(SPORT)
SALON.for_sale(UNIVERSAL)
SALON.for_sale(MINI)
SALON.for_sale(JEEP)
SALON.for_sale(SEDAN)
print()

# Print price list of auto salon:
print("Auto price:")
SALON.price_list()
print()

# Ask user which car he want to buy:
N = input('Enter number of car that you want to buy: ')

# Sale selected car:
SALON.sale(N)
print()

# Print price list of auto salon:
print("Auto price:")
SALON.price_list()
