"""bookcomments.py
Задание:
Создайте класс, описывающий отзыв к книге.
Добавьте в класс книги поле – список отзывов.
Сделайте так, что при выводе книги на экран
при помощи функции print также будут выводиться отзывы к ней.
 """
from datetime import date

class Book:
    """Create class book"""
    def __init__(self, author, name, year, genre):
        """Consrtuctor
        author attribute
        book name attribute
        year of publication attribute
        genre attribute
        empty comments list
        """
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre
        self.comments = []

    def add_comment(self, comment):
        """Add instances of Comments class to list of book comments"""
        self.comments.append(comment)   #add comment to list

    def __str__(self):
        """Description of book for user"""
        return f'The {self.name} book by {self.author} author. Genre: {self.genre}. Date published of book: {self.year}\nComments: {self.comments}'

class Comments:
    """Create class comments"""
    def __init__(self, comment):
        """Constructor
        comment attribute
        author of comment attribute
        date of comment attribute
        """
        self.comment = comment
        #self.commentator = commentator
        #self.date = date

    def __repr__(self):
        """Representation of comments list"""
        return f'Comments: {self.comment}\n'

#    def __str__(self):
#        """Description of book for user"""
#        return f'The {self.name}'

# Create instances of class Book (books):
master_and_margarita = Book("M.Bulgakov", "Master and Margarita", 2018, "Romance")
paint = Book("P.Gnedich", "World paintal art", 2011, "pirtorial art")
child = Book("G.Doring", "Child from 0 to 6", 1992, "child health")
knitting = Book("M.Maximova", "Knitting alphabet", 2014, "needlework")

#Create instances of class Comments (comments):
good = Comments("Good! from cat, date:2017-2-26")
average = Comments("Average. from dog, date:2018-5-10")
poorly = Comments("Poorly :( from bird, date:2019-9-2")
exellent = Comments("Exellent from bird, date:2016-7-4")
#poorly = Comments("Poorly", "bird", date(2019, 9, 2))

#Add comments to book
master_and_margarita.add_comment(good)
master_and_margarita.add_comment(exellent)
paint.add_comment(average)
knitting.add_comment(poorly)

#Print books
print(master_and_margarita)
print(paint)
print(knitting)