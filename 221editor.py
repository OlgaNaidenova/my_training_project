"""221editor.py
Задание:
Создайте класс Editor, который содержит методы view_document и edit_document.
Пусть метод edit_document выводит на экран информацию о том,
что редактирование документов недоступно для бесплатной версии.
Создайте подкласс ProEditor, в котором данный метод будет переопределён.
Введите с клавиатуры лицензионный ключ и, если он корректный,
создайте экземпляр класса ProEditor, иначе Editor.
Вызовите методы просмотра и редактирования документов.
"""

class Editor():
    """Create class Editor"""
    def edit_document(self):
        """edit_document base method"""
        print ('Document editing is not available for the free version')

    def view_document(self):
        """view_document base method"""
        print('Viewed')

class ProEditor(Editor):
    """Create subclass ProEditor"""
    def edit_document(self):
        """edit_document method"""
        print ('Document editing is available')

#Print message for user
key = int(input('Enter license key:'))
CORRECT_KEY = 123

#Create instance of class according to entered key from user
if CORRECT_KEY == key:
    c = ProEditor()     # Create instance of subclass
else:
    c = Editor()    # Create instance of baseclass

#Call methods
c.edit_document()
c.view_document()
