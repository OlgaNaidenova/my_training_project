"""Temperature_with_property.py
Ознакомьтесь с декоратором property в Python.
Создайте класс, описывающий температуру и позволяющий задавать
и получать температуру по шкале Цельсия и Фаренгейта,
причём данные могут быть заданы в одной шкале, а получены в другой.
"""

class Temperature:
    """Create class Temperature"""
    def __init__(self):
        """Constructor
        temp attribute"""
        self._c = 0

    @property
    def c(self):
        """Print saved Сelsius temperature in Сelsius"""
        return self._c

    @c.setter
    def c(self, value):
        """The temperature entered in Сelsius.
        Save this temperature in Сelsius"""
        self._c = value

    @property
    def f(self):
        """Print saved Сelsius temperature in Fahrenheit"""
        return (self._c*9/5)+32    #Сelsius to Fahrenheit

    @f.setter
    def f(self, value):
        """The temperature entered in Fahrenheit.
        Save this temperature in Сelsius"""
        self._c = (value-32)*5/9    #Fahrenheit to Сelsius

t = Temperature()

#Enter 20C
t.c = 20
#Print entered temperature in Сelsius
print(t.c, "C")
#Convert entered temperature to Fahrenheit
print(t.f, "F")
print()

#Enter 68F
t.f = 68
#Convert entered temperature to Сelsius
print(t.c, "C")
#Print entered temperature in Fahrenheit
print(t.f, "F")
print()
