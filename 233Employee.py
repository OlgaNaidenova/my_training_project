"""233Emploee.py
Задание 3:
Опишите класс сотрудника, который включает в себя такие поля,
как имя, фамилия, отдел и год поступления на работу.
 Конструктор должен генерировать исключение, если заданы неправильные данные.
 Введите список работников с клавиатуры.
 Выведите всех сотрудников, которые были приняты после заданного года.
"""

class Employee:
    """Create class Employee"""
    def print_employee(self):
        """print_employee method"""
        try:
            #Verify that name and surname have letters, department and year have digits
            if self.name.isalpha() and self.surname.isalpha() and self.department.isdigit() and self.year.isdigit():
                print(self.name, self.surname, self.department, self.year)
            else:
                # Generate Exception
                raise DataError("error")
        except Exception as e:
            # Call data_error method of DataError class
            print("Error for employee: ", self.name)
            e.data_error()

    def year(self, y):
        """year method to print employees which came after entered year"""
        if int(self.year) > y:                      #Verify that emploeyee year > entered year
            print(self.name, self.surname)          #print name and surname of employee

class DataError(Exception):
    """Create class DataError"""
    def data_error(self):
        """data_error method to print error for user"""
        print('Name or Surname should be letters. Department or Year should be digit')

#Create instances of Employee class from userinput
nina = Employee()
nina.name = input("Enter employee name: ")
nina.surname = input("Enter employee surname: ")
nina.department = input("Enter employee department: ")
nina.year = input("Enter year when employee came: ")
print()

nik = Employee()
nik.name = input("Enter employee name: ")
nik.surname = input("Enter employee surname: ")
nik.department = input("Enter employee department: ")
nik.year = input("Enter year when employee came: ")
print()

nata = Employee()
nata.name = input("Enter employee name:")
nata.surname = input("Enter employee surname:")
nata.department = input("Enter employee department:")
nata.year = input("Enter year when employee came:")
print()

#Print all employees
print("Employees:")
Employee.print_employee(nina)
Employee.print_employee(nik)
Employee.print_employee(nata)
print()

#Aks user to enter year
Y = int(input("Enter year: "))                      #Enter year from user
print("Employees which came after " + str(Y))       #Print message for user
#print employees which came after entere year by call year method
Employee.year(nina, Y)
Employee.year(nik, Y)
Employee.year(nata, Y)

